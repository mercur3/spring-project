package com.example.springproject.usecases.project;

import com.example.springproject.usecases.project.requests.ProjectUpdateForm;
import com.example.springproject.usecases.task.requests.TaskForm;
import com.example.springproject.usecases.task.requests.TaskUpdateForm;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

import java.security.Principal;

public interface HandleProject {
    Result<ErrorKind, ErrorKind> update(ProjectUpdateForm updateForm, Principal principal);

    Result<ErrorKind, ErrorKind> createTask(TaskForm form, Principal principal);

    Result<ErrorKind, ErrorKind> updateTask(TaskUpdateForm form, Principal principal);
}
