package com.example.springproject.usecases.project.response;

import com.example.springproject.adapters.gateways.data.ProjectData;
import com.example.springproject.adapters.gateways.data.UserData;
import com.example.springproject.entities.Task;
import com.example.springproject.usecases.task.response.TaskResponse;

import java.util.List;

public record ProjectResponse(
        long id,
        String name,
        String owner,
        List<String> members,
        List<TaskResponse> tasks
) {
    public static ProjectResponse from(ProjectData projectData) {
        var id = projectData.getId();
        var name = projectData.getName();
        var owner = projectData.getOwner().getUsername();
        var members = projectData.getUsers().stream().map(UserData::getUsername).toList();
        var tasks = projectData.getTasks()
                .stream()
                .map(task -> TaskResponse.fromTask(Task.fromData(task)))
                .toList();

        return new ProjectResponse(id, name, owner, members, tasks);
    }
}
