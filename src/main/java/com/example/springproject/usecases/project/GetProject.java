package com.example.springproject.usecases.project;

import com.example.springproject.usecases.project.response.ProjectResponse;

import java.util.Optional;

public interface GetProject {
    Optional<ProjectResponse> byId(long projectId);
}
