package com.example.springproject.usecases.project.requests;

import com.example.springproject.usecases.project.response.ProjectResponse;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class ProjectUpdateForm {
    private long id;

    @NotBlank(message = "Cannot be empty")
    @Size(min = 3, message = "Must have at least 3 characters")
    private String name;

    @NotBlank(message = "Cannot be empty")
    @Size(min = 3, message = "Must have at least 3 characters")
    private String newUser;

    public static ProjectUpdateForm from(ProjectResponse project) {
        var projectUpdateForm = new ProjectUpdateForm();

        projectUpdateForm.setId(project.id());
        projectUpdateForm.setName(project.name());
        projectUpdateForm.setNewUser("");
        return projectUpdateForm;
    }
}
