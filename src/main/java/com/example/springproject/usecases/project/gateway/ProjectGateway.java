package com.example.springproject.usecases.project.gateway;

import com.example.springproject.adapters.gateways.data.UserData;
import com.example.springproject.usecases.project.response.ProjectResponse;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

import java.util.Optional;

public interface ProjectGateway {
    Optional<ProjectResponse> findById(long id);

    Result<ErrorKind, ErrorKind> update(long projectId, String projectName, UserData newUser);
}
