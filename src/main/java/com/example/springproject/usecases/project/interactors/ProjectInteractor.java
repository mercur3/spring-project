package com.example.springproject.usecases.project.interactors;

import com.example.springproject.adapters.gateways.data.TaskData;
import com.example.springproject.usecases.project.GetProject;
import com.example.springproject.usecases.project.HandleProject;
import com.example.springproject.usecases.project.gateway.ProjectGateway;
import com.example.springproject.usecases.project.requests.ProjectUpdateForm;
import com.example.springproject.usecases.project.response.ProjectResponse;
import com.example.springproject.usecases.task.gateways.TaskGateway;
import com.example.springproject.usecases.task.requests.TaskForm;
import com.example.springproject.usecases.task.requests.TaskUpdateForm;
import com.example.springproject.usecases.user.gateways.UserGateway;
import com.gitlab.mercur3.jrusty.result.Err;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

import java.security.Principal;
import java.util.Optional;

public class ProjectInteractor implements GetProject, HandleProject {
    private final ProjectGateway projectGateway;
    private final UserGateway userGateway;
    private final TaskGateway taskGateway;

    public ProjectInteractor(ProjectGateway projectGateway, UserGateway userGateway, TaskGateway taskGateway) {
        this.projectGateway = projectGateway;
        this.userGateway = userGateway;
        this.taskGateway = taskGateway;
    }

    @Override
    public Optional<ProjectResponse> byId(long projectId) {
        return projectGateway.findById(projectId);
    }

    @Override
    public Result<ErrorKind, ErrorKind> update(ProjectUpdateForm updateForm, Principal principal) {
        var id = updateForm.getId();
        var proj = projectGateway.findById(id);
        if (proj.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        var project = proj.get();
        var ownerName = project.owner();
        if (!userOwnsProject(project, principal)) {
            return new Err<>(ErrorKind.PERMISSION_DENIED);
        }

        var newUserName = updateForm.getNewUser();
        var newMember = userGateway.findUserData(newUserName);
        if (newMember.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }
        if (newUserName.equals(ownerName)) {
            return new Err<>(ErrorKind.ILLEGAL_STATE);
        }
        return projectGateway.update(id, project.name(), newMember.get());
    }

    @Override
    public Result<ErrorKind, ErrorKind> createTask(TaskForm form, Principal principal) {
        var id = form.getProjectId();
        var proj = projectGateway.findById(id);
        if (proj.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        var project = proj.get();
        if (!userOwnsProject(project, principal)) {
            return new Err<>(ErrorKind.PERMISSION_DENIED);
        }

        var username = form.getAssignee();
        var assignee = userGateway.findUserData(username);
        if (assignee.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        return taskGateway.createTask(form, assignee.get());
    }

    @Override
    public Result<ErrorKind, ErrorKind> updateTask(TaskUpdateForm form, Principal principal) {
        var id = form.getTaskId();
        var opt = taskGateway.findTaskData(id);
        if (opt.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        var task = opt.get();
        if (!userCanEditTask(principal, task)) {
            return new Err<>(ErrorKind.PERMISSION_DENIED);
        }
        return taskGateway.updateTask(form);
    }

    /// PRIVATE

    private boolean userOwnsProject(ProjectResponse response, Principal principal) {
        return response.owner()
                .equals(principal.getName());
    }

    private boolean userCanEditTask(Principal principal, TaskData taskData) {
        return taskData.getAssignee()
                .getProjects()
                .stream()
                .map(project -> project.getOwner().getUsername())
                .anyMatch(owner -> owner.equals(principal.getName()));
    }
}
