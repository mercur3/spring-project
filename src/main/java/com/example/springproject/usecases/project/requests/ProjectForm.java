package com.example.springproject.usecases.project.requests;

import com.example.springproject.adapters.gateways.data.ProjectData;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class ProjectForm {
    @NotBlank(message = "Cannot be empty")
    @Size(min = 3, message = "Must have at least 3 characters")
    private String name;

    @NotBlank(message = "Must be an existing user")
    private String owner;

    public void setName(String name) {
        this.name = name.trim();
    }

    public ProjectData toProject() {
        var project = new ProjectData();

        project.setName(name);
        return project;
    }
}
