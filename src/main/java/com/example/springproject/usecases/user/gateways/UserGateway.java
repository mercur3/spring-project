package com.example.springproject.usecases.user.gateways;

import com.example.springproject.adapters.gateways.data.UserData;
import com.example.springproject.entities.User;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

import java.util.Optional;

public interface UserGateway {
    Optional<User> findByUsername(String username);

    Optional<UserData> findUserData(String username);

    Result<ErrorKind, ErrorKind> save(UserData user);
}
