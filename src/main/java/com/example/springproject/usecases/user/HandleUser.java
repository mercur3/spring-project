package com.example.springproject.usecases.user;

import com.example.springproject.usecases.user.requests.RegistrationForm;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

public interface HandleUser {
    Result<ErrorKind, ErrorKind> save(RegistrationForm form);
}
