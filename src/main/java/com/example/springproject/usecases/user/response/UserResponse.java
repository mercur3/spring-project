package com.example.springproject.usecases.user.response;

import com.example.springproject.entities.User;
import com.example.springproject.usecases.task.response.TaskResponse;

import java.util.List;

public record UserResponse(
        String username,
        String password,
        List<UserProjectResponse> projects,
        List<TaskResponse> tasks
) {
    public static UserResponse fromUser(User u) {
        var username = u.getUsername();
        var password = u.getPassword();
        var projects = u.getProjects()
                .stream()
                .map(project -> {
                    boolean isOwner = project.getOwner().equals(u.getUsername());
                    return new UserProjectResponse(project.getId(), project.getName(), isOwner);
                })
                .toList();
        var tasks = u.getTasks()
                .stream()
                .map(TaskResponse::fromTask)
                .toList();
        return new UserResponse(username, password, projects, tasks);
    }
}
