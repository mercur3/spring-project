package com.example.springproject.usecases.user.requests;

import com.example.springproject.adapters.gateways.data.UserData;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class RegistrationForm {
    @NotBlank(message = "Cannot be be empty")
    @Size(min = 5, message = "Must have length >= 5")
    private String username;

    @NotBlank(message = "Cannot be be empty")
    @Size(min = 3, message = "Must have length >= 3")
    private String password;

    public void setUsername(String username) {
        this.username = username.trim();
    }

    public UserData toUser(PasswordEncoder passwordEncoder) {
        var user = new UserData();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));

        return user;
    }
}
