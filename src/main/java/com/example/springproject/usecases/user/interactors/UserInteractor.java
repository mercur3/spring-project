package com.example.springproject.usecases.user.interactors;

import com.example.springproject.config.security.SecurityConfig;
import com.example.springproject.entities.Role;
import com.example.springproject.usecases.role.gateways.RoleGateway;
import com.example.springproject.usecases.user.GetUser;
import com.example.springproject.usecases.user.HandleUser;
import com.example.springproject.usecases.user.gateways.UserGateway;
import com.example.springproject.usecases.user.requests.RegistrationForm;
import com.example.springproject.usecases.user.response.UserResponse;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

import java.util.Optional;

public class UserInteractor implements GetUser, HandleUser {
    private final UserGateway userGateway;
    private final RoleGateway roleGateway;

    public UserInteractor(UserGateway userGateway, RoleGateway roleGateway) {
        this.userGateway = userGateway;
        this.roleGateway = roleGateway;
    }

    @Override
    public Optional<UserResponse> byUsername(String username) {
        return userGateway.findByUsername(username)
                .map(UserResponse::fromUser);
    }

    @Override
    public Result<ErrorKind, ErrorKind> save(RegistrationForm form) {
        var user = form.toUser(SecurityConfig.ENCODER);
        var role_user = roleGateway.get(Role.USER);

        user.add(role_user);
        return userGateway.save(user);
    }
}
