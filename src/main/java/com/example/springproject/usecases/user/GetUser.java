package com.example.springproject.usecases.user;

import com.example.springproject.usecases.user.response.UserResponse;

import java.util.Optional;

public interface GetUser {
    Optional<UserResponse> byUsername(String username);
}
