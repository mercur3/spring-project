package com.example.springproject.usecases.user.response;

public record UserProjectResponse(long projectId, String projectName, boolean isOwner) {}
