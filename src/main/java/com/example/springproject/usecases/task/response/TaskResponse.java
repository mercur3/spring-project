package com.example.springproject.usecases.task.response;

import com.example.springproject.entities.Comment;
import com.example.springproject.entities.Task;

import java.util.Set;


public record TaskResponse(
        long taskId,
        String description,
        boolean done,
        String assignee,
        long projectId,
        Set<Comment> comments  /// FIXME list of comment response
) {
    public static TaskResponse fromTask(Task t) {
        var taskId = t.getId();
        var desc = t.getDescription();
        var done = t.isDone();
        var assignee = t.getAssignee();
        var projectId = t.getProject();
        var comments = t.getComments();

        return new TaskResponse(taskId, desc, done, assignee, projectId, comments);
    }
}
