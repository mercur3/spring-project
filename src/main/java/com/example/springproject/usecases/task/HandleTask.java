package com.example.springproject.usecases.task;

import com.example.springproject.usecases.comment.requests.CommentForm;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

import java.security.Principal;

public interface HandleTask {
    Result<ErrorKind, ErrorKind> closeTask(long taskId, Principal principal);

    Result<ErrorKind, ErrorKind> addComment(CommentForm form, Principal principal);

}
