package com.example.springproject.usecases.task.interactors;

public enum TaskStatus {
    FINISHED,
    NOT_FINISHED,
}
