package com.example.springproject.usecases.task.interactors;

import com.example.springproject.adapters.gateways.data.TaskData;
import com.example.springproject.usecases.comment.gateway.CommentGateway;
import com.example.springproject.usecases.comment.requests.CommentForm;
import com.example.springproject.usecases.task.GetTask;
import com.example.springproject.usecases.task.HandleTask;
import com.example.springproject.usecases.task.gateways.TaskGateway;
import com.example.springproject.usecases.task.response.TaskResponse;
import com.gitlab.mercur3.jrusty.result.Err;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public class TaskInteractor implements GetTask, HandleTask {
    private final TaskGateway taskGateway;
    private final CommentGateway commentGateway;

    public TaskInteractor(TaskGateway taskGateway, CommentGateway commentGateway) {
        this.taskGateway = taskGateway;
        this.commentGateway = commentGateway;
    }

    @Override
    public Optional<TaskResponse> byId(long taskId) {
        return taskGateway.findById(taskId).map(TaskResponse::fromTask);
    }

    @Override
    public List<TaskResponse> bySearchCriteria(
            Optional<TaskStatus> status,
            Optional<String> user,
            Optional<String> keyword
    ) {
        return taskGateway.findTaskBySearchCriteria(status, user, keyword)
                .stream()
                .map(TaskResponse::fromTask)
                .toList();
    }

    @Override
    public Result<ErrorKind, ErrorKind> closeTask(long taskId, Principal principal) {
        var opt = taskGateway.findTaskData(taskId);
        if (opt.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        var task = opt.get();
        if (!userHasAccess(task, principal)) {
            return new Err<>(ErrorKind.PERMISSION_DENIED);
        }

        return taskGateway.closeTask(taskId);
    }

    @Override
    public int userAccessLevel(Principal principal, long taskId) {
        var task = taskGateway.findById(taskId);
        if (task.isEmpty()) {
            System.out.printf("Task %d does not exists\n", taskId);
            return 0;
        }

        var isAssigned = task.get().getAssignee().equals(principal.getName());
        if (isAssigned) {
            return 2;
        }
        return 1;
    }

    @Override
    public boolean exists(long id) {
        return taskGateway.exists(id);
    }

    @Override
    public Result<ErrorKind, ErrorKind> addComment(CommentForm form, Principal principal) {
        var id = form.getTaskId();
        var opt = taskGateway.findTaskData(id);
        if (opt.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        var task = opt.get();
        if (!userHasAccess(task, principal)) {
            return new Err<>(ErrorKind.PERMISSION_DENIED);
        }
        return commentGateway.addComment(task, form.intoCommentData());
    }

    // PRIVATE

    private boolean userHasAccess(TaskData task, Principal principal) {
        return task.getAssignee()
                .getUsername()
                .equals(principal.getName());
    }
}
