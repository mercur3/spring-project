package com.example.springproject.usecases.task.gateways;

import com.example.springproject.adapters.gateways.data.TaskData;
import com.example.springproject.adapters.gateways.data.TaskData_;
import com.example.springproject.adapters.gateways.data.UserData_;
import com.example.springproject.usecases.task.interactors.TaskStatus;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;

public final class TaskSpecification {
    public static Specification<TaskData> createTaskSpecification(
            Optional<TaskStatus> status,
            Optional<String> username,
            Optional<String> keyword
    ) {
        return hasStatus(status)
                .and(userSimilarTo(username))
                .and(containsKeyword(keyword));
    }

    public static Specification<TaskData> hasStatus(Optional<TaskStatus> status) {
        return (root, query, criteriaBuilder) -> status.map(
                        s -> switch (s) {
                            case FINISHED -> criteriaBuilder.isTrue(root.get(TaskData_.done));
                            case NOT_FINISHED -> criteriaBuilder.isFalse(root.get(TaskData_.done));
                        })
                .orElse(null);
    }

    public static Specification<TaskData> userSimilarTo(Optional<String> username) {
        return (root, query, criteriaBuilder) -> username.map(uname -> {
            var user_task = root.join(TaskData_.assignee);
            var normalizeDb = criteriaBuilder.lower(user_task.get(UserData_.username));
            var normalizeInput = matchLower(uname);
            return criteriaBuilder.like(normalizeDb, normalizeInput);
        }).orElse(null);
    }

    public static Specification<TaskData> containsKeyword(Optional<String> keyword) {
        return (root, query, criteriaBuilder) -> keyword.map(str -> {
            var normalizeDb = criteriaBuilder.lower(root.get(TaskData_.description));
            var normalizeInput = matchLower(str);
            return criteriaBuilder.like(normalizeDb, normalizeInput);
        }).orElse(null);
    }

    /// PRIVATE

    private static String matchLower(String str) {
        return "%" + str + "%";
    }
}
