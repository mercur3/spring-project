package com.example.springproject.usecases.task;

import com.example.springproject.usecases.task.interactors.TaskStatus;
import com.example.springproject.usecases.task.response.TaskResponse;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface GetTask {
    Optional<TaskResponse> byId(long taskId);

    List<TaskResponse> bySearchCriteria(
            Optional<TaskStatus> status,
            Optional<String> user,
            Optional<String> keyword
    );

    /**
     * Gives the access level of user in this task.
     *
     * @return <ul>
     * 2 - User is assigned to this task. Can view it and close it.
     * 1 - User is in project. Can only view.
     * 0 - User not related to the task. Not allow to view it.
     * </ul>
     */
    int userAccessLevel(Principal principal, long taskId);

    boolean exists(long id);
}
