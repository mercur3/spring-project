package com.example.springproject.usecases.task.requests;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class TaskUpdateForm {
    private long taskId;

    @NotBlank(message = "Cannot be empty")
    @Size(min = 5, message = "Minimal length required 5 characters")
    private String taskDescription;

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription.trim();
    }
}
