package com.example.springproject.usecases.task.requests;

import com.example.springproject.adapters.gateways.data.TaskData;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class TaskForm {
    @NotBlank(message = "Description required")
    @Size(min = 5, message = "Minimum 5 chars")
    private String description;

    @NotBlank(message = "Cannot be empty")
    private String assignee;

    private long projectId;
    private List<String> members;

    public TaskForm(long projectId, List<String> members) {
        this.projectId = projectId;
        this.members = members;
    }

    public TaskData intoTask() {
        var task = new TaskData();

        task.setDescription(description);
        return task;
    }
}
