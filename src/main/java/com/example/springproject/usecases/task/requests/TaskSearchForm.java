package com.example.springproject.usecases.task.requests;

import com.example.springproject.usecases.task.interactors.TaskStatus;
import lombok.Data;

@Data
public class TaskSearchForm {
    private TaskStatus status;
    private String user;
    private String keyword;
}
