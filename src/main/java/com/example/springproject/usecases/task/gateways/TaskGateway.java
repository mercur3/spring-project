package com.example.springproject.usecases.task.gateways;

import com.example.springproject.adapters.gateways.data.TaskData;
import com.example.springproject.adapters.gateways.data.UserData;
import com.example.springproject.entities.Task;
import com.example.springproject.usecases.task.interactors.TaskStatus;
import com.example.springproject.usecases.task.requests.TaskForm;
import com.example.springproject.usecases.task.requests.TaskUpdateForm;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

import java.util.List;
import java.util.Optional;

public interface TaskGateway {
    Optional<Task> findById(long taskId);

    Optional<TaskData> findTaskData(long id);

    Result<ErrorKind, ErrorKind> closeTask(long taskId);

    List<Task> findTaskBySearchCriteria(
            Optional<TaskStatus> status,
            Optional<String> user,
            Optional<String> keyword
    );

    boolean exists(long taskId);

    Result<ErrorKind, ErrorKind> createTask(TaskForm form, UserData assignee);

    Result<ErrorKind, ErrorKind> updateTask(TaskUpdateForm form);
}
