package com.example.springproject.usecases.comment.requests;

import com.example.springproject.adapters.gateways.data.CommentData;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class CommentForm {
    private long taskId;

    @NotBlank
    @Size(min = 5, message = "At least 5 chars required")
    private String comment;

    public void setComment(String comment) {
        this.comment = comment.trim();
    }

    public CommentData intoCommentData() {
        var commentData = new CommentData();

        commentData.setComment(comment);
        commentData.setTime(LocalDateTime.now());
        return commentData;
    }
}
