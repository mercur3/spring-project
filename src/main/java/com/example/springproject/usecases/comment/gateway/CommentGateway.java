package com.example.springproject.usecases.comment.gateway;

import com.example.springproject.adapters.gateways.data.CommentData;
import com.example.springproject.adapters.gateways.data.TaskData;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Result;

public interface CommentGateway {
    Result<ErrorKind, ErrorKind> addComment(TaskData task, CommentData comment);
}
