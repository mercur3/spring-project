package com.example.springproject.usecases.role.gateways;

import com.example.springproject.adapters.gateways.data.RoleData;
import com.example.springproject.entities.Role;

public interface RoleGateway {
    RoleData get(Role r);
}
