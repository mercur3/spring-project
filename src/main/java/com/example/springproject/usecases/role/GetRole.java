package com.example.springproject.usecases.role;

import com.example.springproject.entities.Role;

public interface GetRole {
    Role get(Role role);
}
