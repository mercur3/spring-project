package com.example.springproject.adapters.controllers;

import com.example.springproject.config.GlobalConfigs;
import com.example.springproject.usecases.project.GetProject;
import com.example.springproject.usecases.project.HandleProject;
import com.example.springproject.usecases.project.requests.ProjectUpdateForm;
import com.example.springproject.usecases.task.requests.TaskForm;
import com.example.springproject.usecases.task.requests.TaskUpdateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/project")
public class ProjectController {
    private final GetProject getProject;
    private final HandleProject handleProject;

    @Autowired
    public ProjectController(GetProject getProject, HandleProject handleProject) {
        this.getProject = getProject;
        this.handleProject = handleProject;
    }

    @GetMapping
    public String dashboard(
            @RequestParam("project_id") long id,
            Model model,
            Principal principal
    ) {
        var opt = getProject.byId(id);
        if (opt.isEmpty()) {
            return "/user";
        }

        var project = opt.get();
        model.addAttribute("proj", project);
        return "project/dashboard";
    }

    @GetMapping("/edit")
    public String edit(
            @RequestParam("project_id") long id,
            Model model
    ) {
        var proj = getProject.byId(id);
        var formStr = "projectUpdate";

        if (proj.isEmpty()) {
            return "redirect:/project";
        }
        if (!model.containsAttribute(formStr)) {
            model.addAttribute(formStr, ProjectUpdateForm.from(proj.get()));
        }
        return "project/update-form";
    }

    @PostMapping("/update")
    public String update(
            @Valid @ModelAttribute("projectUpdate") ProjectUpdateForm updateForm,
            BindingResult bindingResult,
            Principal principal,
            RedirectAttributes redirectAttributes
    ) {
        var id = updateForm.getId();
        var notSuccessUrl = "redirect:/project/edit?project_id=" + id;
        var successUrl = "redirect:/project?project_id=" + id;
        var formStr = "projectUpdate";

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    GlobalConfigs.BindingResult_FULL_CLASS_NAME + ".projectUpdate",
                    bindingResult
            );
            redirectAttributes.addFlashAttribute(formStr, updateForm);
            return "redirect:/project/edit?project_id=" + id;
        }
        var res = handleProject.update(updateForm, principal);
        if (res.isOk()) {
            return successUrl;
        }
        redirectAttributes.addFlashAttribute(formStr, updateForm);
        return notSuccessUrl;
    }

    @GetMapping("/add-task")
    public String addTask(
            @RequestParam("project_id") long id,
            Model model
    ) {
        var project = getProject.byId(id);
        if (project.isEmpty()) {
            System.out.printf("No project with id %d\n", id);
            return "redirect:/user";
        }

        var task = new TaskForm(id, project.get().members());
        model.addAttribute("task", task);
        return "project/create-task-form";
    }

    @PostMapping("/create-task")
    public String createTask(
            @Valid @ModelAttribute("task") TaskForm form,
            BindingResult bindingResult,
            Principal principal
    ) {
        if (bindingResult.hasErrors()) {
            return "/project/create-task-form";
        }
        var res = handleProject.createTask(form, principal);
        if (res.isErr()) {
            return "/project/create-task-form";
        }
        return "redirect:/user";
    }

    @PostMapping("/update-task")
    public String updateTask(
            @Valid @ModelAttribute("form") TaskUpdateForm form,
            BindingResult bindingResult,
            Principal principal
    ) {
        var id = form.getTaskId();
        if (bindingResult.hasErrors()) {
            return "redirect:/project/update-task-form?task_id=" + id;
        }
        var res = handleProject.updateTask(form, principal);
        if (res.isErr()) {
            return "redirect:/project/update-task-form?task_id=" + id;
        }
        return "redirect:/user";
    }
}
