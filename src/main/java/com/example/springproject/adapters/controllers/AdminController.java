package com.example.springproject.adapters.controllers;

import com.example.springproject.other.UserService;
import com.example.springproject.usecases.project.requests.ProjectForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final UserService service;

    @Autowired
    public AdminController(UserService service) {
        this.service = service;
    }

    @GetMapping
    public String adminSite() {
        return "admin";
    }

    @GetMapping("/add-project")
    public String addProject(Model model) {
        model.addAttribute("form", new ProjectForm());
        return "admin/add-project";
    }

    @PostMapping("/register-project")
    public String registerProject(
            @Valid @ModelAttribute("form") ProjectForm form,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            return "admin/add-project";
        }
        if (!service.addNewProject(form)) {
            System.out.printf("User [%s] does not exist\n", form.getOwner());
            return "admin/add-project";
        }

        System.out.printf("Added: %s", form);
        return "admin";
    }
}
