package com.example.springproject.adapters.controllers;

import com.example.springproject.config.GlobalConfigs;
import com.example.springproject.usecases.comment.requests.CommentForm;
import com.example.springproject.usecases.task.GetTask;
import com.example.springproject.usecases.task.HandleTask;
import com.example.springproject.usecases.task.interactors.TaskStatus;
import com.example.springproject.usecases.task.requests.TaskSearchForm;
import com.example.springproject.usecases.user.GetUser;
import com.example.springproject.usecases.user.HandleUser;
import com.example.springproject.usecases.user.requests.RegistrationForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;

@Controller
public class WebController {
    private final GetUser getUser;
    private final HandleUser handleUser;
    private final GetTask getTask;
    private final HandleTask handleTask;

    @Autowired
    public WebController(GetUser getUser, HandleUser handleUser, GetTask getTask, HandleTask handleTask) {
        this.getUser = getUser;
        this.handleUser = handleUser;
        this.getTask = getTask;
        this.handleTask = handleTask;
    }

    @GetMapping("/signup")
    public String signup(Model model) {
        model.addAttribute("form", new RegistrationForm());
        return "signup-form";
    }

    @PostMapping("/register")
    public String register(
            @Valid @ModelAttribute("form") RegistrationForm form,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            return "signup-form";
        }

        var res = handleUser.save(form);
        if (res.isErr()) {
            return "signup-form";
        }
        return "redirect:/";
    }

    @GetMapping("/user")
    public String welcome(Model model, Principal principal) {
        var user = getUser.byUsername(principal.getName()).get();
        model.addAttribute("user", user);
        return "user";
    }

    @GetMapping("/user/task")
    public String taskView(
            @RequestParam("task_id") long id,
            Model model,
            Principal principal
    ) {
        var opt = getTask.byId(id);
        if (opt.isEmpty()) {
            return "redirect:/user";
        }

        var accessLevel = getTask.userAccessLevel(principal, id);
        if (accessLevel <= 0) {
            System.out.printf("User %s not allowed to view task %d\n", principal.getName(), id);
            return "redirect:/user";
        }

        var task = opt.get();
        var taskCommentStr = "taskComment";

        model.addAttribute("task", task);
        model.addAttribute("accessLevel", accessLevel);
        if (!model.containsAttribute(taskCommentStr)) {
            var comment = new CommentForm();
            comment.setTaskId(task.taskId());
            model.addAttribute("taskComment", comment);
        }
        return "task-view";
    }

    @GetMapping("/user/task/close")
    public String closeTask(
            @RequestParam("task_id") long id,
            Principal principal
    ) {
        var res = handleTask.closeTask(id, principal);
        if (res.isOk()) {
            return "redirect:/user";
        }
        return "redirect:/user/task?task_id=" + id;
    }

    @PostMapping("/user/task/add-comment")
    public String addComment(
            @Valid @ModelAttribute("taskComment") CommentForm form,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes,
            Principal principal
    ) {
        var returnAddress = "redirect:/user/task?task_id=" + form.getTaskId();
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    GlobalConfigs.BindingResult_FULL_CLASS_NAME + ".taskComment",
                    bindingResult
            );
            redirectAttributes.addFlashAttribute("taskComment", form);
            return returnAddress;
        }

        var res = handleTask.addComment(form, principal);
        if (res.isErr()) {
            System.out.printf("Could not add comment\n Error: %s\n", res.err().get());
        }
        return returnAddress;
    }

    @GetMapping("/search/task")
    public String searchTask(
            @RequestParam(required = false) Optional<TaskStatus> status,
            @RequestParam(required = false) Optional<String> user,
            @RequestParam(required = false) Optional<String> keyword,
            Model model
    ) {
        var searchResult = getTask.bySearchCriteria(status, user, keyword);

        model.addAttribute("statuses", TaskStatus.values());
        model.addAttribute("form", new TaskSearchForm());
        model.addAttribute("results", searchResult);
        return "search-task";
    }
}
