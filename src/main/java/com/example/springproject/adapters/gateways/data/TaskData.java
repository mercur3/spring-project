package com.example.springproject.adapters.gateways.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "task")
@NoArgsConstructor
@Getter
@Setter
public class TaskData {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private long id;

    private String description;
    private boolean done = false;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserData assignee;

    @ManyToOne
    @JoinColumn(name = "project")
    private ProjectData project;

    @OneToMany(mappedBy = "task")
    private Set<CommentData> comments = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskData taskData = (TaskData) o;
        return id == taskData.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void assignToThisTask(UserData userData) {
        setAssignee(userData);
        userData.getTasks().add(this);
    }

    public void addComment(CommentData c) {
        comments.add(c);
        c.setTask(this);
    }
}
