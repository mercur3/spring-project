package com.example.springproject.adapters.gateways.project;

import com.example.springproject.adapters.gateways.data.ProjectData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectData, Long> {
}
