package com.example.springproject.adapters.gateways.comment;

import com.example.springproject.adapters.gateways.data.CommentData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<CommentData, Long> {
}
