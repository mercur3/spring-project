package com.example.springproject.adapters.gateways.task;

import com.example.springproject.adapters.gateways.data.TaskData;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<TaskData, Long>, JpaSpecificationExecutor<TaskData> {
    List<TaskData> findAll(@Nullable Specification<TaskData> specification);
}
