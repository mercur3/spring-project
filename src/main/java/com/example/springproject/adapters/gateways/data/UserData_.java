package com.example.springproject.adapters.gateways.data;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UserData.class)
public class UserData_ {
    public static volatile SingularAttribute<UserData, String> username;
}
