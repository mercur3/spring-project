package com.example.springproject.adapters.gateways.role;

import com.example.springproject.adapters.gateways.data.RoleData;
import com.example.springproject.entities.Role;
import com.example.springproject.usecases.role.gateways.RoleGateway;

public class RoleDataMapper implements RoleGateway {
    private final RoleRepository repository;

    public RoleDataMapper(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public RoleData get(Role r) {
        return repository.findByRole(r.roleName);
    }
}
