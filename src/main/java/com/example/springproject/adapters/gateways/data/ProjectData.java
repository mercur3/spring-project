package com.example.springproject.adapters.gateways.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "project")
@NoArgsConstructor
@Getter
@Setter
public class ProjectData {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private long id;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name = "owner")
    private UserData owner;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "user_project",
            joinColumns = @JoinColumn(name = "project"),
            inverseJoinColumns = @JoinColumn(name = "user")
    )
    private Set<UserData> users = new HashSet<>();

    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL
    )
    private Set<TaskData> tasks = new HashSet<>();

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void addUser(UserData userData) {
        users.add(userData);
    }

    public void removeUser(UserData userData) {
        users.remove(userData);
        userData.getProjects().remove(this);
    }
}
