package com.example.springproject.adapters.gateways.user;

import com.example.springproject.adapters.gateways.data.UserData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserData, String> {
    Optional<UserData> findByUsername(String username);
}
