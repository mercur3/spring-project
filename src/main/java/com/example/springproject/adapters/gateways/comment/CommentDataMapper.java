package com.example.springproject.adapters.gateways.comment;

import com.example.springproject.adapters.gateways.data.CommentData;
import com.example.springproject.adapters.gateways.data.TaskData;
import com.example.springproject.usecases.comment.gateway.CommentGateway;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Ok;
import com.gitlab.mercur3.jrusty.result.Result;

public class CommentDataMapper implements CommentGateway {
    private final CommentRepository repository;

    public CommentDataMapper(CommentRepository repository) {
        this.repository = repository;
    }

    @Override
    public Result<ErrorKind, ErrorKind> addComment(TaskData task, CommentData comment) {
        task.addComment(comment);
        repository.save(comment);
        return new Ok<>(ErrorKind.EMPTY);
    }
}
