package com.example.springproject.adapters.gateways.data;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
public class UserData {
    @Id
    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(
            mappedBy = "owner",
            cascade = CascadeType.ALL
    )
    private List<ProjectData> ownedProjects;

    @OneToMany(mappedBy = "assignee")
    private Set<TaskData> tasks = new HashSet<>();

    @ManyToMany(mappedBy = "users")
    private Set<ProjectData> projects = new HashSet<>();

    @ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
    private Set<RoleData> roles = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserData userData = (UserData) o;
        return username.equals(userData.username);
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    public void add(RoleData roleData) {
        roles.add(roleData);
        roleData.getUsers().add(this);
    }
}
