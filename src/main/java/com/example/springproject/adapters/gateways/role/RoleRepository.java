package com.example.springproject.adapters.gateways.role;

import com.example.springproject.adapters.gateways.data.RoleData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleData, String> {
    RoleData findByRole(String role);
}
