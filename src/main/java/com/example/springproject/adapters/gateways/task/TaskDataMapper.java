package com.example.springproject.adapters.gateways.task;

import com.example.springproject.adapters.gateways.comment.CommentRepository;
import com.example.springproject.adapters.gateways.data.TaskData;
import com.example.springproject.adapters.gateways.data.UserData;
import com.example.springproject.entities.Task;
import com.example.springproject.usecases.task.gateways.TaskGateway;
import com.example.springproject.usecases.task.gateways.TaskSpecification;
import com.example.springproject.usecases.task.interactors.TaskStatus;
import com.example.springproject.usecases.task.requests.TaskForm;
import com.example.springproject.usecases.task.requests.TaskUpdateForm;
import com.gitlab.mercur3.jrusty.result.Err;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Ok;
import com.gitlab.mercur3.jrusty.result.Result;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskDataMapper implements TaskGateway {
    private final TaskRepository taskRepository;
    private final CommentRepository commentRepository;

    public TaskDataMapper(TaskRepository taskRepository, CommentRepository commentRepository) {
        this.taskRepository = taskRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public Optional<Task> findById(long taskId) {
        return taskRepository.findById(taskId)
                .map(Task::fromData);
    }

    @Override
    public Optional<TaskData> findTaskData(long id) {
        return taskRepository.findById(id);
    }

    @Override
    public Result<ErrorKind, ErrorKind> closeTask(long taskId) {
        var opt = taskRepository.findById(taskId);
        if (opt.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        var task = opt.get();
        task.setDone(true);
        taskRepository.save(task);
        return new Ok<>(ErrorKind.EMPTY);
    }

    @Override
    public List<Task> findTaskBySearchCriteria(
            Optional<TaskStatus> status,
            Optional<String> user,
            Optional<String> keyword
    ) {
        return taskRepository.findAll(TaskSpecification.createTaskSpecification(status, user, keyword))
                .stream()
                .map(Task::fromData)
                .collect(Collectors.toList());
    }

    @Override
    public boolean exists(long taskId) {
        return taskRepository.existsById(taskId);
    }

    @Override
    public Result<ErrorKind, ErrorKind> createTask(TaskForm form, UserData assignee) {
        var task = form.intoTask();
        task.assignToThisTask(assignee);
        taskRepository.save(task);
        return new Ok<>(ErrorKind.EMPTY);
    }

    @Override
    public Result<ErrorKind, ErrorKind> updateTask(TaskUpdateForm form) {
        var opt = findTaskData(form.getTaskId());
        if (opt.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        var task = opt.get();
        task.setDescription(form.getTaskDescription());
        taskRepository.save(task);
        return new Ok<>(ErrorKind.EMPTY);
    }
}
