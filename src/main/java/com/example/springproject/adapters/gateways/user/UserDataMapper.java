package com.example.springproject.adapters.gateways.user;

import com.example.springproject.adapters.gateways.data.UserData;
import com.example.springproject.entities.User;
import com.example.springproject.usecases.user.gateways.UserGateway;
import com.gitlab.mercur3.jrusty.result.Err;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Ok;
import com.gitlab.mercur3.jrusty.result.Result;

import java.util.Optional;

public class UserDataMapper implements UserGateway {
    private final UserRepository userRepository;

    public UserDataMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findByUsername(String username) {
        var userData = findUserData(username);
        if (userData.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(User.fromData(userData.get()));
    }

    @Override
    public Optional<UserData> findUserData(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Result<ErrorKind, ErrorKind> save(UserData user) {
        if (userRepository.existsById(user.getUsername())) {
            return new Err<>(ErrorKind.ALREADY_EXISTS);
        }
        userRepository.save(user);
        return new Ok<>(ErrorKind.EMPTY);
    }
}
