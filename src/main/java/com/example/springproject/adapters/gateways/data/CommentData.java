package com.example.springproject.adapters.gateways.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "comment")
@Getter
@Setter
@NoArgsConstructor
public class CommentData {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private long id;

    @ManyToOne
    @JoinColumn(name = "task_id")
    private TaskData task;

    @Column
    private String comment;

    @Column
    private LocalDateTime time;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentData commentData = (CommentData) o;
        return id == commentData.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
