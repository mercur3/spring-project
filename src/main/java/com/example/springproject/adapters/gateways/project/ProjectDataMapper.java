package com.example.springproject.adapters.gateways.project;

import com.example.springproject.adapters.gateways.data.UserData;
import com.example.springproject.usecases.project.gateway.ProjectGateway;
import com.example.springproject.usecases.project.response.ProjectResponse;
import com.gitlab.mercur3.jrusty.result.Err;
import com.gitlab.mercur3.jrusty.result.ErrorKind;
import com.gitlab.mercur3.jrusty.result.Ok;
import com.gitlab.mercur3.jrusty.result.Result;

import java.util.Optional;

public class ProjectDataMapper implements ProjectGateway {
    private final ProjectRepository projectRepository;

    public ProjectDataMapper(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Optional<ProjectResponse> findById(long id) {
        return projectRepository.findById(id)
                .map(ProjectResponse::from);
    }

    @Override
    public Result<ErrorKind, ErrorKind> update(long id, String name, UserData data) {
        var opt = projectRepository.findById(id);
        if (opt.isEmpty()) {
            return new Err<>(ErrorKind.NOT_FOUND);
        }

        var project = opt.get();
        project.setName(name);
        project.addUser(data);
        projectRepository.save(project);
        return new Ok<>(ErrorKind.EMPTY);
    }
}
