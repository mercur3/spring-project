package com.example.springproject.adapters.gateways.data;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(TaskData.class)
public class TaskData_ {
    public static volatile SingularAttribute<TaskData, Boolean> done;
    public static volatile SingularAttribute<TaskData, String> description;
    public static volatile SingularAttribute<TaskData, UserData> assignee;
}
