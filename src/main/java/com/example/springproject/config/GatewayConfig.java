package com.example.springproject.config;

import com.example.springproject.adapters.gateways.comment.CommentDataMapper;
import com.example.springproject.adapters.gateways.comment.CommentRepository;
import com.example.springproject.adapters.gateways.project.ProjectDataMapper;
import com.example.springproject.adapters.gateways.project.ProjectRepository;
import com.example.springproject.adapters.gateways.role.RoleDataMapper;
import com.example.springproject.adapters.gateways.role.RoleRepository;
import com.example.springproject.adapters.gateways.task.TaskDataMapper;
import com.example.springproject.adapters.gateways.task.TaskRepository;
import com.example.springproject.adapters.gateways.user.UserDataMapper;
import com.example.springproject.adapters.gateways.user.UserRepository;
import com.example.springproject.usecases.comment.gateway.CommentGateway;
import com.example.springproject.usecases.project.gateway.ProjectGateway;
import com.example.springproject.usecases.role.gateways.RoleGateway;
import com.example.springproject.usecases.task.gateways.TaskGateway;
import com.example.springproject.usecases.user.gateways.UserGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {
    @Bean
    @Autowired
    public UserGateway userGateway(UserRepository userRepository) {
        return new UserDataMapper(userRepository);
    }

    @Bean
    @Autowired
    public TaskGateway taskGateway(
            TaskRepository taskRepository,
            CommentRepository commentRepository
    ) {
        return new TaskDataMapper(taskRepository, commentRepository);
    }

    @Bean
    @Autowired
    public RoleGateway roleGateway(RoleRepository roleRepository) {
        return new RoleDataMapper(roleRepository);
    }

    @Bean
    @Autowired
    public ProjectGateway projectGateway(ProjectRepository projectRepository) {
        return new ProjectDataMapper(projectRepository);
    }

    @Bean
    @Autowired
    public CommentGateway commentGateway(CommentRepository repository) {
        return new CommentDataMapper(repository);
    }
}
