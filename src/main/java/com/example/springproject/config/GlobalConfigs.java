package com.example.springproject.config;

import org.springframework.validation.BindingResult;

public final class GlobalConfigs {
    public static final String BindingResult_FULL_CLASS_NAME = BindingResult.class.getCanonicalName();

    /// PRIVATE

    private GlobalConfigs() {}
}
