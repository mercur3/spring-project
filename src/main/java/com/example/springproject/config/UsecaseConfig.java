package com.example.springproject.config;

import com.example.springproject.usecases.comment.gateway.CommentGateway;
import com.example.springproject.usecases.project.GetProject;
import com.example.springproject.usecases.project.gateway.ProjectGateway;
import com.example.springproject.usecases.project.interactors.ProjectInteractor;
import com.example.springproject.usecases.role.gateways.RoleGateway;
import com.example.springproject.usecases.task.GetTask;
import com.example.springproject.usecases.task.gateways.TaskGateway;
import com.example.springproject.usecases.task.interactors.TaskInteractor;
import com.example.springproject.usecases.user.GetUser;
import com.example.springproject.usecases.user.HandleUser;
import com.example.springproject.usecases.user.gateways.UserGateway;
import com.example.springproject.usecases.user.interactors.UserInteractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UsecaseConfig {
    @Bean
    @Autowired
    public GetUser getUser(UserGateway userGateway, RoleGateway roleGateway) {
        return new UserInteractor(userGateway, roleGateway);
    }

    @Bean
    @Autowired
    public HandleUser handleUser(UserGateway userGateway, RoleGateway roleGateway) {
        return new UserInteractor(userGateway, roleGateway);
    }

    @Bean
    @Autowired
    public GetTask getTask(TaskGateway taskGateway, CommentGateway commentGateway) {
        return new TaskInteractor(taskGateway, commentGateway);
    }

    @Bean
    @Autowired
    public GetProject getProject(
            ProjectGateway projectGateway,
            UserGateway userGateway,
            TaskGateway taskGateway
    ) {
        return new ProjectInteractor(projectGateway, userGateway, taskGateway);
    }
}
