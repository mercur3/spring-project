package com.example.springproject.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    public static final PasswordEncoder ENCODER = NoOpPasswordEncoder.getInstance();
    private final UserDetailsService userDetailsService;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(ENCODER);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/signup")
                    .permitAll()
                .antMatchers("/user", "/user/**", "/project", "/search/**")
                    .authenticated()
                .antMatchers("/admin", "/admin/**")
                    .hasRole("ADMIN")
                .antMatchers("/project/**")
                    .hasRole("OWNER")
                .and()
                .formLogin()
                    .defaultSuccessUrl("/user")
                .and()
                .logout()
                    .logoutSuccessUrl("/");
    }
}
