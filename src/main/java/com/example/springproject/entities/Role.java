package com.example.springproject.entities;

import java.util.Optional;

public enum Role {
    ADMIN("ROLE_ADMIN"),
    OWNER("ROLE_OWNER"),
    USER("ROLE_USER");

    public final String roleName;

    Role(String roleName) {
        this.roleName = roleName;
    }

    public static Optional<Role> from(String str) {
        var out = switch (str) {
            case "ROLE_ADMIN" -> Role.ADMIN;
            case "ROLE_OWNER" -> Role.OWNER;
            case "ROLE_USER" -> Role.USER;
            default -> null;
        };
        return Optional.ofNullable(out);
    }
}
