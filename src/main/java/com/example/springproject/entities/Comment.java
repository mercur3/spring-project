package com.example.springproject.entities;

import com.example.springproject.adapters.gateways.data.CommentData;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Comment {
    private long id;
    private long task;
    private String comment;
    private LocalDateTime time;

    public static Comment fromData(CommentData data) {
        var comment = new Comment();
        comment.setId(data.getId());
        comment.setTask(data.getTask().getId());
        comment.setComment(data.getComment());
        comment.setTime(data.getTime());

        return comment;
    }
}
