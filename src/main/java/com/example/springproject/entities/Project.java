package com.example.springproject.entities;

import com.example.springproject.adapters.gateways.data.ProjectData;
import com.example.springproject.adapters.gateways.data.UserData;
import lombok.Data;

import java.util.Set;
import java.util.stream.Collectors;

@Data
public class Project {
    private long id;
    private String name;
    private String owner;
    private Set<String> users;
    private Set<Task> tasks;

    public static Project fromData(ProjectData data) {
        var project = new Project();
        project.setId(data.getId());
        project.setName(data.getName());
        project.setOwner(data.getOwner().getUsername());
        project.setUsers(data.getUsers().stream().map(UserData::getUsername).collect(Collectors.toSet()));
        project.setTasks(data.getTasks().stream().map(Task::fromData).collect(Collectors.toSet()));

        return project;
    }
}
