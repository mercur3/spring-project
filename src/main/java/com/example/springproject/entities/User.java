package com.example.springproject.entities;

import com.example.springproject.adapters.gateways.data.ProjectData;
import com.example.springproject.adapters.gateways.data.UserData;
import lombok.Data;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class User {
    private String username;
    private String password;
    private List<Long> ownedProjects;
    private Set<Task> tasks;
    private Set<Project> projects;
    private Set<Role> roles;

    public static User fromData(UserData data) {
        var usr = new User();
        usr.setUsername(data.getUsername());
        usr.setPassword(usr.getPassword());
        usr.setOwnedProjects(data.getOwnedProjects().stream().map(ProjectData::getId).collect(Collectors.toList()));
        usr.setTasks(data.getTasks().stream().map(Task::fromData).collect(Collectors.toSet()));
        usr.setProjects(data.getProjects().stream().map(Project::fromData).collect(Collectors.toSet()));
        usr.setRoles(data.getRoles().stream().map(el -> Role.from(el.getRole()).get()).collect(Collectors.toSet()));

        return usr;
    }
}
