package com.example.springproject.entities;

import com.example.springproject.adapters.gateways.data.TaskData;
import lombok.Data;

import java.util.Set;
import java.util.stream.Collectors;

@Data
public class Task {
    private long id;
    private String description;
    private boolean done;
    private String assignee;
    private long project;
    private Set<Comment> comments;

    public static Task fromData(TaskData data) {
        var t = new Task();
        t.setId(data.getId());
        t.setDescription(data.getDescription());
        t.setDone(data.isDone());
        t.setAssignee(data.getAssignee().getUsername());
        t.setProject(data.getProject().getId());
        t.setComments(data.getComments().stream().map(Comment::fromData).collect(Collectors.toSet()));

        return t;
    }
}
