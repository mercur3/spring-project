package com.example.springproject.other;

import com.example.springproject.adapters.gateways.project.ProjectRepository;
import com.example.springproject.adapters.gateways.role.RoleRepository;
import com.example.springproject.adapters.gateways.user.UserRepository;
import com.example.springproject.usecases.project.requests.ProjectForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final ProjectRepository projectRepository;

    @Autowired
    public UserService(
            UserRepository userRepository,
            RoleRepository roleRepository,
            ProjectRepository projectRepository
    ) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.projectRepository = projectRepository;
    }


    /// FIXME does not belong here
    @Transactional
    public boolean addNewProject(ProjectForm form) {
        var usr = userRepository.findByUsername(form.getOwner());
        if (usr.isEmpty()) {
            return false;
        }

        var user = usr.get();
        var project = form.toProject();

        project.setOwner(user);
        project.addUser(user);
        projectRepository.save(project);

        user.add(roleRepository.findByRole("ROLE_OWNER"));
        userRepository.save(user);
        return true;
    }
}
