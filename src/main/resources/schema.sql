drop database if exists issue_tracker;
create database issue_tracker;
USE issue_tracker;


CREATE TABLE `users` (
	`username` VARCHAR(50) NOT NULL,
	`password` VARCHAR(36) NOT NULL,

	primary key(`username`)
) ENGINE=INNODB;

CREATE TABLE `role` (
	`role` VARCHAR(55) NOT NULL,

	primary key(`role`)
) ENGINE=INNODB;

CREATE TABLE `user_role` (
	`user` VARCHAR(50) NOT NULL,
	`role` VARCHAR(55) NOT NULL,

	primary key(`user`, `role`),
	foreign key(`user`) references `users`(`username`),
	foreign key(`role`) references `role`(`role`)
) ENGINE=INNODB;

create table `project` (
    `id` bigint not null auto_increment,
    `name` varchar(50) not null,
    `owner` varchar(50) not null,

    primary key(`id`),
    foreign key(`owner`) references `users`(`username`)
) ENGINE=INNODB;

create table `participant` (
    `user` varchar(50) not null,
    `project` bigint not null,

    primary key(`user`, `project`),
    foreign key(`user`) references `users`(`username`),
    foreign key(`project`) references `project`(`id`)
) ENGINE=INNODB;

create table `task` (
    `id` bigint not null auto_increment,
    `description` varchar(255) not null,
    `done` tinyint(1) not null default '1',
    `project` bigint not null,

    primary key(`id`),
    foreign key(`project`) references `project`(`id`)
) ENGINE=INNODB;

create table `involved` (
    `user` varchar(50) not null,
    `task` bigint not null,

    primary key(`user`, `task`),
    foreign key(`user`) references `users`(`username`),
    foreign key(`task`) references `task`(`id`)
) ENGINE=INNODB;
